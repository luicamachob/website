const serviceID = "service_w2fwqj4";
const templateID = "template_hnfmtrr";

function sendMail(){
    var ele = document.getElementsByName('date-option');
    for(i = 0; i < ele.length; i++) {
        if(ele[i].checked)
        var dateOption = ele[i].placeholder;
    }

    var date = document.getElementById('calendar').value;

    messageBody = "opcion: " + dateOption + ", fecha: " + date

    var params = {
        message: messageBody
    }

    emailjs
    .send(serviceID, templateID, params)
    .then((res) => {
        showAlert(res);
    })
    .catch((err => alert("Hubo un error a la hora de mandar tus respuestas.")));
}

function showAlert(res) {
    console.log(res)
        if (res.status == 200) {
          alert("Gracias Sandy, ya se mandaron tus respuestas.")
        } else {
          alert("Hubo un error a la hora de mandar tus respuestas.")
        }
}

function dateDenied() {
    if (confirm("¿Segura que tu respuesta es no? Todavía puedes cambiar de opinión si presionas cancelar")) {
        var params = {
            message: "Ha rechazado la salida"
        }

        emailjs
        .send(serviceID, templateID, params)
        .then((res) => {
            alert("Rechazaste la salida")
        })
        .catch((err => alert("Hubo un error a la hora de mandar tus respuestas.")));
    }
}